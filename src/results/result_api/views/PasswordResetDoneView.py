from django.contrib.auth.hashers import make_password
from django.core.validators import validate_email
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from result_api import models


class PasswordResetDoneView(APIView):
    permission_classes = [AllowAny, ]

    def post(self, request):

        if 'email' in request.data:
            email = request.data['email'].strip()
        else:
            return Response({"message": "provide email number"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        if 'ticket' in request.data:
            ticket = request.data['ticket'].lower().strip()
        else:
            return Response({"message": "provide ticket number"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        if 'password' in request.data:
            password = request.data['password']

        else:
            return Response({"message": "provide a password"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        try:
            validate_email(email)
            try:
                user = models.UserProfile.objects.get(email=email)
                stored_ticket = user.ticket.lower().strip()
                if stored_ticket == ticket:
                    if password is not None:
                        if len(password) > 7:
                            password = make_password(password)
                            print(password)
                            user.password = password
                            user.save()
                            return Response({"message": "email verified"},
                                            status=status.HTTP_200_OK)
                        else:
                            return Response({"message": "password must have at least 8 characters"},
                                            status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
                    else:
                        return Response({"message": "provide a password"},
                                        status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

                else:
                    return Response({"message": "ticket not matched"},
                                    status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
            except Exception:
                return Response({"message": "email is not registered"},
                                status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        except ValidationError:
            return Response({"message": "provide a valid email address"},
                            status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)