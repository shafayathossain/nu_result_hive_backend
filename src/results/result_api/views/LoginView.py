from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_jwt.views import ObtainJSONWebToken

from src.results.result_api import permission, models


class LoginViewSet(ObtainJSONWebToken):
    permission_classes = [AllowAny, permission.IsUpdateProfile, ]

    def post(self, request):
        print(request)
        email = request.data['email']
        data = {}

        queryset_of_email = models.Email.objects.filter(email=email)
        for obj in queryset_of_email:
            id = obj.id
            is_activate = obj.is_activate

        data = request
        data.data['email'] = id

        if is_activate:

            response = super(LoginViewSet, self).post(data)
            print(response.data)
            return Response({"token": response.data['token']})
        else:
            return Response({"message": "email not verified"})