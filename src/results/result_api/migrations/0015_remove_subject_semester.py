# Generated by Django 2.0.4 on 2018-08-23 14:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('result_api', '0014_subject_semester'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subject',
            name='semester',
        ),
    ]
