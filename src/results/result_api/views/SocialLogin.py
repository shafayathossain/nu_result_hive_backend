from urllib.error import HTTPError
from urllib.request import urlopen

import requests
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

from result_api import models


class Social_Login(APIView):
    permission_classes = [AllowAny, ]

    def get(self, request):
        # print(request.query_params)
        if 'token' in request.query_params:
            token = request.query_params['token']
        else:
            return Response({"message": "Token not found"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        if 'email' in request.query_params:
            email = request.query_params['email']
        else:
            return Response({"message": "Provide an email"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        user_query_object = models.UserProfile.objects.filter(email=email)

        query_email = ""

        for i in user_query_object:
            query_email = i.email

        if query_email is "":

            try:
                url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + token
                urlopen(url)

                response = requests.get(url).json()
                if response['email'] == email:
                    models.UserProfile.objects.create(email=response['email'],
                                                      name=response['name'],
                                                      username=response['name'],
                                                      is_active=1,
                                                      reg_no="",
                                                      contact_no="",
                                                      ticket="")

                    user_query_object = models.UserProfile.objects.filter(email=email)
                    user = None
                    for i in user_query_object:
                        user = i

                    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                    payload = jwt_payload_handler(user)
                    jwt_token = jwt_encode_handler(payload)
                    print(jwt_token)

                    return Response({"token": jwt_token, "user": user.id, "email": user.email, "name": user.name,
                                     'contact_no': user.contact_no, 'reg_no': user.reg_no}, status=status.HTTP_200_OK)
                else:
                    return Response({"message": "Email doesn't matched"},
                                    status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
            except HTTPError:
                try:
                    url = 'https://graph.facebook.com/me?fields=email,name&access_token=' + token
                    urlopen(url)

                    response = requests.get(url).json()
                    if response['email'] == email:
                        models.UserProfile.objects.create(email=response['email'],
                                                          name=response['name'],
                                                          username=response['name'],
                                                          is_active=1,
                                                          reg_no="",
                                                          contact_no="",
                                                          ticket="")

                        user_query_object = models.UserProfile.objects.filter(email=email)
                        user = None
                        for i in user_query_object:
                            user = i

                        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                        payload = jwt_payload_handler(user)
                        jwt_token = jwt_encode_handler(payload)
                        print(jwt_token)

                        return Response({"token": jwt_token, "user": user.id, "email": user.email, "name": user.name,
                                         'contact_no': user.contact_no, 'reg_no': user.reg_no},
                                        status=status.HTTP_200_OK)
                    else:
                        return Response({"message": "Email doesn't matched"},
                                        status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
                except HTTPError:
                    return Response({"message": "User not verified"},
                                    status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        else:
            try:
                url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + token
                urlopen(url)
                response = requests.get(url).json()
                if response['email'] == email:
                    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                    user = None
                    for i in user_query_object:
                        user = i

                    if user.is_active == 0:
                        user_query_object.update(is_active=1)
                    if user.username == "":
                        user_query_object.update(username=response['name'])
                    payload = jwt_payload_handler(user)
                    jwt_token = jwt_encode_handler(payload)
                    print(jwt_token)
                    return Response({"token": jwt_token, "user": user.id, "email": user.email, "name": user.name,
                                     'contact_no': user.contact_no, 'reg_no': user.reg_no}, status=status.HTTP_200_OK)
                else:
                    return Response({"message": "Email doesn't matched"},
                                    status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
            except HTTPError:
                try:
                    url = 'https://graph.facebook.com/me?fields=email,name&access_token=' + token
                    urlopen(url)
                    response = requests.get(url).json()
                    if response['email'] == email:
                        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                        user = None
                        for i in user_query_object:
                            user = i

                        if user.is_active == 0:
                            user_query_object.update(is_active=1)
                        if user.username == "":
                            user_query_object.update(username=response['name'])
                        payload = jwt_payload_handler(user)
                        jwt_token = jwt_encode_handler(payload)
                        print(jwt_token)
                        return Response({"token": jwt_token, "user": user.id, "email": user.email, "name": user.name,
                                         'contact_no': user.contact_no, 'reg_no': user.reg_no},
                                        status=status.HTTP_200_OK)
                    else:
                        return Response({"message": "Email doesn't matched"},
                                        status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
                except HTTPError:
                    return Response({"message": "User not verified"},
                                    status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)