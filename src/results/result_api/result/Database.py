import MySQLdb as mysql
from . import Information

connection = 0
cursor = 0

def executeQuery(query, tuple, cursor):
    try:
        cursor.execute(query, tuple)
    except mysql.Error as e:
        print(e)

    row = cursor.fetchone()
    print(row)
    return row[0]

def executeInsertion(insert, tuple, cursor, connection):
    cursor.execute(insert, tuple)
    connection.commit()

def databaseActivity(information):

    global connection
    global cursor
    try:
        connection = mysql.connect(user='root',
                                   password='shafa33',
                                   host='127.0.0.1',
                                   database='result-test-2', )
    except mysql.Error as e:
        print(e)
    cursor = connection.cursor()

    get_id_of_college_query = "SELECT id FROM college WHERE code = %s"
    insert = "INSERT INTO college (name, code) VALUES (%s, %s)"
    collegeName = information.getCollegeName()
    collegeCode = information.getCollegeCode()
    collegeID = 0
    try:
        collegeID = executeQuery(get_id_of_college_query, (collegeCode,), cursor)
    except StopIteration:
        executeInsertion(insert, (collegeName, collegeCode,), cursor, connection)
        collegeID = executeQuery(get_id_of_college_query, (collegeCode,), cursor)

    get_id_of_student_query = "SELECT id FROM student WHERE registration_number = %s"
    insert = "INSERT INTO student (registration_number, name, session, college_id) VALUES (%s, %s, %s, %s)"
    studentName = information.getName()
    registrationNumber = information.getRegistrationNumber()
    session = information.getSession()
    studentId = 0
    try:
        studentId = executeQuery(get_id_of_student_query, (registrationNumber,), cursor)
    except StopIteration:
        executeInsertion(insert, (registrationNumber, studentName, session, collegeID,), cursor, connection)
        studentId = executeQuery(get_id_of_student_query, (registrationNumber,), cursor)

    get_credit_of_subject_query = "SELECT credit FROM subject WHERE code = %s"
    credits = []
    subjects = information.getSubjects()
    for subject in subjects:
        credits.append(executeQuery(get_credit_of_subject_query, (subject,), cursor))
    get_id_of_semester_query = "SELECT id FROM semester WHERE student_id = %s AND semester = %s"
    insert = "INSERT INTO semester (student_id, semester, sgpa, grade, is_fail) VALUES (%s, %s, %s, %s, %s)"
    semester = information.getSemester()
    sgpa = information.getCGPA(credits)
    grade = information.getGrade(sgpa)
    is_fail = information.isFail()
    semesterId = 0
    try:
        semesterId = executeQuery(get_id_of_semester_query, (studentId, semester,), cursor)
        if semesterId is not None:
            newQuery = "SELECT sgpa FROM semester WHERE student_id = %s AND semester = %s"
            currentCGPA = executeQuery(newQuery, (studentId, semester,), cursor)
            if(currentCGPA < sgpa):
                updateCGPA = "UPDATE semester SET sgpa = %s WHERE id = %s"
                updateGrade = "UPDATE semester SET grade = %s WHERE id = %s"
                updateFailStatus = "UPDATE semester SET is_fail = %s WHERE id = %s"
                executeInsertion(updateCGPA, (sgpa, semesterId,), cursor, connection)
                executeInsertion(updateGrade, (grade, semesterId,), cursor, connection)
                executeInsertion(updateFailStatus, (is_fail, semesterId,), cursor, connection)
                semesterId = executeQuery(get_id_of_semester_query, (studentId, semester,), cursor)
    except StopIteration:
        executeInsertion(insert, (studentId, semester, sgpa, grade, is_fail), cursor, connection)
        semesterId = executeQuery(get_id_of_semester_query, (studentId, semester,), cursor)

    get_id_of_subject_query = "SELECT id FROM subject WHERE code = %s"
    ids = []
    for subject in subjects:
        ids.append(executeQuery(get_id_of_subject_query, (subject,), cursor))
    get_id_of_result_query = "SELECT id FROM result WHERE subject_id = %s AND semester_id = %s"
    insert = "INSERT INTO result (semester_id, subject_id, result) VALUES(%s, %s, %s)"
    results = information.getResults()
    for subjectId, result in zip(ids, results):
        try:
            resultId = executeQuery(get_id_of_result_query, (subjectId, semesterId,), cursor)
            if resultId is not None:
                newQuery = "SELECT result FROM result WHERE id = %s"
                currentResult = executeQuery(newQuery, (resultId,), cursor)
                if(information.gpaByGrade[currentResult] < information.gpaByGrade[result]):
                    update = "UPDATE result SET result = %s WHERE id = %s"
                    executeInsertion(update,(result, resultId,), cursor, connection)
        except StopIteration:
            executeInsertion(insert, (semesterId, subjectId, result), cursor, connection)
    databaseClose()

def databaseClose():
    global connection
    global cursor
    connection.close()

