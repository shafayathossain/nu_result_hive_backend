from django.db.models import Q
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import generics, viewsets, status
from rest_framework.permissions import AllowAny
from result_api import models, serializer
from rest_framework.response import Response


class SearchViewSet(generics.mixins.ListModelMixin,
                    viewsets.GenericViewSet):

    def get_queryset(self):
        if 'q' in self.request.query_params and len(self.request.query_params['q'])>0:
            keyword = self.request.query_params['q']
            return models.student.objects.filter(Q(name__icontains=keyword) | Q(registration_number__icontains=keyword))
        else:
            return models.student.objects.filter(Q(name__icontains='    ') | Q(registration_number__icontains='    '))

    def retrieve(self):
        if 'q' not in self.request.query_params:

            return Response({"message": "No keyword found"})

    serializer_class = serializer.SearchSerializer
    permission_classes = [AllowAny]


