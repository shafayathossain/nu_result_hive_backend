import threading
import urllib

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from result_api import models
from result_api.result.Main import main


class Update(APIView):

    def get(self, request):
        link = request.query_params['link']

        link_size = len(link)

        if link[link_size - 4:link_size] != ".txt":
            return Response({"message": "not a valid url"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
        try:
            urllib.request.urlretrieve(link, 'result.txt')
        except:
            return Response({"message": "not a valid url"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        if link[0:7] == 'http://':
            link = link[7:]

        if link[0:4] == 'www.':
            link = link[4:]

        if link[0:8] == 'nu.ac.bd' or link[0:9]== 'nubd.info':
            # fullfilename = os.path.join('../result_api/ResultFiles', 'result.txt')
            urllib.request.urlretrieve("http://www." + link, 'result.txt')

            # t1 = threading.Thread(target=main())
            # t1.setDaemon(False)
            about_db = models.about_db.objects.all()
            if not about_db.exists():
                row = models.about_db(is_busy=False)
                row.save()

            is_busy = False
            for row in about_db:
                is_busy = row.is_busy
            t = ''

            if not is_busy:
                print("here")
                models.about_db.objects.filter(id=1).update(is_busy=True)
                t1 = threading.Thread(target=main)
                t1.setDaemon(False)
                t1.start()
            # connection = mysql.connect(user='shafayat_hossain',
            #                              passwd='hawk.2520',
            #                              host='localhost',
            #                              db='shafayat_result')
            # cursor = connection.cursor()
            # query = "UPDATE `db_busy` SET `is_busy`=0 WHERE 1"
            # cursor.execute(query)
            # connection.commit()
            # connection.close()
            print("out")
            return Response(
                {"message": "Thanks for your contribution. This may takes 20 minutes to update result." + t},
                status=status.HTTP_200_OK)



        else:
            return Response({"message": "not a valid url"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

    pass