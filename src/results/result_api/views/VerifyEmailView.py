from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from result_api import permission
from result_api import models
from result_api import serializer


class VerifyEmail(APIView):
    permission_classes = [AllowAny, permission.IsUpdateProfile, ]

    def post(self, request, **kwargs):

        data = {}
        ticket = ''
        id = 0
        if 'email' in request.data:
            data['email'] = request.data['email'].rstrip().lower()
        else:
            return Response({"detail": "enter the email"},
                            status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        if 'ticket' in request.data:
            data['ticket'] = request.data['ticket'].lower().replace(" ", "")
        else:
            return Response({"detail": "enter the ticket number"},
                            status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        queryset = models.UserProfile.objects.filter(email = data['email'])

        for obj in queryset:
            ticket = obj.ticket.lower().replace(" ", "")
            id = obj.id

        if data['email'] == '' or data['ticket'] == '':
            return Response({"detail": "enter valid ticket and email"},
                            status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        print(ticket)
        print(data['ticket'])
        if ticket == data['ticket']:
            data['is_active'] = True
            serializerC = serializer.FirstStepSerializer(data=data)
            return Response(data={"id": id}, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "token do not match"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)