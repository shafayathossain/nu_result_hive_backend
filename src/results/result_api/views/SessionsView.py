from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from result_api import models


class Sessions(APIView):

    permission_classes = [AllowAny,]

    def get(self, request, *args, **kwargs):
        queryset = models.student.objects.values('session').distinct().order_by('session')
        return Response(queryset, status=status.HTTP_200_OK)
