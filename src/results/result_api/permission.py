from rest_framework.permissions import BasePermission
from rest_framework import permissions

class POSTAccessPermission(BasePermission):
    message = 'You cannot do this operation'
    my_safe_method = ('POST')

    def has_permission(self, request, view):
        print(request.method)
        if request.method in self.my_safe_method:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        return request.user.is_superuser

class GETOnlyPermission(BasePermission):
    message = 'You cannot do this operation'
    my_safe_method = ['GET', ]
    def has_permission(self, request, view):
        if request.method in self.my_safe_method:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        return False

class IsUpdateProfile(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return request.user == obj