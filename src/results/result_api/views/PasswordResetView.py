import random

from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMessage
from django.core.validators import validate_email
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from result_api import models


class PasswordResetView(APIView):
    permission_classes = [AllowAny, ]

    def post(self, request):
        email = ''
        user = ''
        if 'email' in request.data:
            email = request.data['email']
            try:
                validate_email(email)
                try:
                    user = models.UserProfile.objects.get(email=email)
                except ObjectDoesNotExist:
                    return Response({"message": "email is not registered"},
                                    status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
                if user.is_active:
                    print(user)
                    string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    ticket = ''
                    for i in range(0, 6):
                        ticket = ticket + random.choice(string)
                    user.ticket = ticket
                    user.save()
                    email = EmailMessage('Email Varification For ',
                                         'Thank you for using Result Hive. Please confirm email address by the ticket number '
                                         'provided following. Without varifying email address you cannot reset password. '
                                         'Provide the number on the ticket number field in the application.\n\n '
                                         'Ticket number: {}\n\nGreating from our team.'.format(
                                             ticket), to=[email])
                    email.send()
                    return Response({"message": "check email"},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({"message": "Email is not registered"},
                                    status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

            except ValidationError:
                return Response({"message": "Provide a valid emaiil"},
                                status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        else:
            return Response({"message": "Provide an email"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)