from django.contrib.auth.hashers import make_password
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from result_api import models
from result_api import serializer

class changeProfile(APIView):

    def post(self, request):
        id = 0
        if 'id' in request.data:
            id = int(request.data['id'])
        else:
            return Response({"detail": "provide profile ID"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
        if request.user.id is id:
            data = {}
            data['id'] = id
            queryset = models.UserProfile.objects.filter(id=id)
            for attr in queryset:
                data['name'] = attr.username
                data['reg_no'] = attr.reg_no
                data['contact_no'] = attr.contact_no
                data['password'] = attr.password

            if 'name' in request.data and len(request.data['name']) > 0:
                data['name'] = request.data['name']
            elif 'name' in request.data and len(request.data['name']) is 0:
                return Response({"detail": "enter name"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
            
            if 'contact_no' in request.data and len(request.data['contact_no']) > 0:
                try:
                    data['contact_no'] = int(request.data['contact_no'])
                    if len(request.data['contact_no'])<11:
                        return Response({"detail": "enter a valid contact number"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
                except Exception:
                    return Response({"detail": "enter a valid contact number"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
            elif 'contact_no' in request.data and len(request.data['contact_no']) is 0:
                data['contact_no'] = request.data['contact_no']
                    

            if 'old_password' in request.data and len(request.data['old_password']) > 0:
                old_password = request.data['old_password']
                new_password = request.data['new_password']
                if len(new_password) < 8 or new_password == None:
                    return Response({"detail": "password must have at least 8 characters"},
                                    status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

                user = models.UserProfile.objects.get(id=id)
                if user.check_password(old_password):
                    data['password'] = make_password(new_password)
                else:
                    return Response({"detail": "old password does not matched with this account"},
                                    status=status.HTTP_401_UNAUTHORIZED)

            if 'reg_no' in request.data and len(request.data['reg_no']) > 0:
                data['reg_no'] = request.data['reg_no']
            elif 'reg_no' in request.data and len(request.data['reg_no']) is 0:
                return Response({"detail": "enter registration number"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

            serializerC = serializer.UserProfileSerializer(data=data)
            serializerC.update(data)
            return Response({"message": "Information saved successfully"})
        else:
            return Response({"message": "your are not authorized"},
                            status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)