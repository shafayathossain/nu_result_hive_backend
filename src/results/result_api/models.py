from django.contrib.auth.models import BaseUserManager
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin

# Create your models here.
class college(models.Model):
    name = models.CharField(max_length=255)
    code = models.IntegerField(unique=True)

    class Meta:
        db_table = 'college'


class subject(models.Model):
    name = models.CharField(max_length=255)
    code = models.IntegerField()
    credit = models.FloatField()

    class Meta:
        db_table = 'subject'


class student(models.Model):
    name = models.CharField(max_length=255)
    registration_number = models.CharField(max_length=255)
    session = models.CharField(max_length=255)
    college = models.ForeignKey(to='college', on_delete=models.CASCADE)

    class Meta:
        db_table = 'student'


class semester(models.Model):
    semester = models.IntegerField()
    student = models.ForeignKey(to='student', on_delete=models.CASCADE)
    sgpa = models.FloatField()
    grade = models.CharField(max_length=255)
    is_fail = models.BooleanField(default=True)

    class Meta:
        db_table = 'semester'


class result(models.Model):
    result = models.CharField(max_length=255)
    semester = models.ForeignKey(to='semester', on_delete=models.CASCADE)
    subject = models.ForeignKey(to='subject', on_delete=models.CASCADE)

    class Meta:
        db_table = 'result'


class UserProfileManager(BaseUserManager):
    def create_user(self, email, name, reg_no=None, contact_no=None, password=None, token=None):
        if not email:
            raise ValueError('User must have an email address')

        email = self.normalize_email(email)
        user = self.model(email=email, name=name, reg_no = reg_no, contact_no = contact_no ,token = token)

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, name, password=None):
        user = self.create_user(email, name, password)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)

        return user

class UserProfile(AbstractBaseUser, PermissionsMixin, models.Model):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=255, default=None)
    username = models.CharField(max_length=255, default=name)
    reg_no = models.CharField(max_length=255, default=None)
    contact_no = models.CharField(max_length=255, default=None)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    ticket = models.CharField(max_length=255, default=None)

    objects = UserProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def get_short_name(self):
        return self.name

    def __str__(self):
        return self.email

class about_db(models.Model):
    is_busy = models.BooleanField(default=False)

    class Meta:
        db_table = 'db_busy'

class about_result(models.Model):
    year = models.IntegerField()
    semester = models.IntegerField()

    class Meta:
        db_table = 'about_result'

# class Email(models.Model):
#     email = models.EmailField(max_length=255, unique=True)
#     token = models.CharField(max_length=255)
#     is_activate = models.BooleanField(default=False)
#
#     class Meta:
#         db_table = 'Email'
