from django.contrib.auth.hashers import make_password
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from result_api import permission, models
from result_api import serializer


class UserProfileViewSet(APIView):
    permission_classes = [AllowAny, permission.IsUpdateProfile, ]

    def get(self, request, *args, **kwargs):
        # print(request.query_params['id'])
        id = kwargs['id']
        data = {}
        queryset = models.UserProfile.objects.filter(id=id)
        if queryset.count()==0:
            return Response({"message": "No user found"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
        for attr in queryset:
            data['id'] = attr.id
            data['email'] = attr.email
            data['name'] = attr.name
            data['reg_no'] = attr.reg_no
            data['contact_no'] = attr.contact_no

        serializerC = serializer.UserProfileSerializer(data=data)
        if hasattr(request.user, 'email'):
            if request.user.email == data['email']:
                return Response(serializerC.initial_data)
            else:
                return Response({"message" : "your are not authorized"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
        else:
            return Response({"message": "your are not authorized"},
                            status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)


    def post(self, request):
        is_active = False
        # with open('print.txt', 'a') as the_file:
        #     the_file.write(str(request.data)+"\n")
        name = request.data['userName']
        password = make_password(request.data['password'])
        if 'reg_no' not in request.data:
            reg_no = ""
        if 'contact_no' not in request.data:
            contact_no = ""
        id = request.data['id']
        data = {}

        query = models.UserProfile.objects.filter(id=id)
        for obj in query:
            print(obj)
            is_active = obj.is_active

        if not is_active:
            data['id'] = id
            data['name'] = name
            data['password'] = password
            data['contact_no'] = contact_no
            data['reg_no'] = reg_no
            serializerC = serializer.UserProfileSerializer(data=data)
            if data['name'] == '':
                return Response({"detail": "enter name"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
            if len(request.data['password']) < 8 or request.data['password']=='':
                return Response({"detail": "password must have at least 8 characters"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
            serializerC.update(data)
            return Response({"message":"registered successfully"})

        else:
            return Response({"detail": "Already registered"}, status=status.HTTP_409_CONFLICT)