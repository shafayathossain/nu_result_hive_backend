from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView


class Default(APIView):
    permission_classes = [AllowAny]

    @staticmethod
    def head(request):
        return Response("ok", status=status.HTTP_200_OK)

