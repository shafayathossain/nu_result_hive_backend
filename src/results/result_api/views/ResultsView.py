from django.db import connection
from django.db.models import Q
from rest_framework import generics, viewsets, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from result_api import serializer
from result_api import models


class ResultsViewSet(generics.mixins.ListModelMixin,
                     viewsets.GenericViewSet):

    def get_permissions(self):

        if 'registration_number' not in self.kwargs:
            self.permission_classes = [AllowAny]
        else:
            self.permission_classes = [IsAuthenticated]

        return super(ResultsViewSet, self).get_permissions()

    def get_queryset(self):
        if 'registration_number' in self.kwargs:
            param = self.kwargs['registration_number']
            student_info = {}
            semester_info = {}
            result_info = {}
            student_info['semesters'] = []
            semester_info['subject'] = []
            queryset = models.result.objects.select_related('semester__student__college', 'subject')\
                .filter(semester__student__registration_number=param)\
                .order_by('semester__semester', 'subject__code')

            current_semester = 0
            for row in queryset:
                print(row.semester.student)
                if current_semester!= row.semester.semester:
                    semester_info = {}
                    semester_info['subject'] = []
                    student_info['semesters'].append(semester_info)

                student_info['name'] = row.semester.student.name
                student_info['college'] = row.semester.student.college.name
                student_info['registration_number'] = row.semester.student.registration_number
                student_info['session'] = row.semester.student.session
                student_info['college_code'] = row.semester.student.college.code

                semester_info['semester'] = row.semester.semester
                semester_info['SGPA'] = row.semester.sgpa
                semester_info['grade'] = row.semester.grade
                semester_info['is_fail'] = 0
                if row.semester.is_fail:
                    semester_info['is_fail'] = 1

                result_info['subject_name'] = row.subject.name
                result_info['subject_code'] = row.subject.code
                result_info['result'] = row.result
                semester_info['subject'].append(result_info.copy())
                current_semester = row.semester.semester

            return student_info

        else:
            keyword = self.request.query_params['search']
            self.serializer_class = serializer.SearchSerializer
            return models.student.objects\
                .filter(Q(name__icontains=keyword) | Q(registration_number__icontains=keyword))\
                .order_by('college')

    serializer_class = serializer.ResultsSerializer
    lookup_field = 'registration_number'

    def retrieve(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset())
        if serializer.instance=={}:
            return Response({"message": "student info not found"}, status=status.HTTP_204_NO_CONTENT)
        return Response(data=serializer.instance)