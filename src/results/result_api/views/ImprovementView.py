from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from result_api import models


class Improvements(APIView):

    def get(self, request, registration_number=-1):

        if registration_number==-1 or len(registration_number)==0:
            return Response({"message": "Enter a valid registration number"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
        student_info = {}
        semester_info = {}
        result_info = {}
        student_info['semesters'] = []
        semester_info['subject'] = []
        queryset = models.result.objects.select_related('semester__student__college', 'subject') \
            .filter(semester__student__registration_number=registration_number) \
            .order_by('semester__semester', 'subject__code')

        current_semester = 0
        for row in queryset:
            print(row.semester.student)
            if current_semester != row.semester.semester:
                semester_info = {}
                semester_info['subject'] = []
                semester_info['total_credit'] = 0
                student_info['semesters'].append(semester_info)


            student_info['name'] = row.semester.student.name
            student_info['college'] = row.semester.student.college.name
            student_info['registration_number'] = row.semester.student.registration_number
            student_info['session'] = row.semester.student.session
            student_info['college_code'] = row.semester.student.college.code

            semester_info['semester'] = row.semester.semester
            semester_info['SGPA'] = row.semester.sgpa
            semester_info['grade'] = row.semester.grade
            semester_info['is_fail'] = 0
            semester_info['total_credit'] += row.subject.credit
            if row.semester.is_fail:
                semester_info['is_fail'] = 1

            result_info['subject_name'] = row.subject.name
            result_info['subject_code'] = row.subject.code
            result_info['credit'] = row.subject.credit
            result_info['result'] = row.result
            semester_info['subject'].append(result_info.copy())
            current_semester = row.semester.semester

        return Response(student_info, status=status.HTTP_200_OK)