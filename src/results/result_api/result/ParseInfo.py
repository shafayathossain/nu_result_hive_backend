import re
from . import Information
from . import Instance
from .Database import databaseActivity

globalSemester = 0
globalCollegeName = 0
globalCollegeCode = 0
globalSubjectCodeList = []

def process(line):
    #print(line)

    '''
    :param line:
    To avoid initialization every time, initialized globally. To use that global variables, declare those by 'global variable_name'

    We split every line into list of strings(:var listOfString) by [ ]*[|][ ]* to get result easily.
    If the word 'SEMESTER' found, then index(40th) character is the number of semester.
    If the word 'College' found, then substring[9:13] will be college code and substring[15:end] will be college name.
    If :var listOfString has more than 4 elements, then it must be a line from result table. So:
        If listOfString[1] consists of 'ROLL NO' then it is the attribute name line of the table.
            Get the subject code list from there. Sublist[5:end] will be subject code list
        Else that line consists of result of a information.
            Get registration number from index(2) of listOfString
            Get session from index(3) of listOfString
            Get information's name from index(4) of listOfString
            Get information's grade list from sublist of listOfString[5:end]


    :return:
    '''
    registrationNumber = 0
    studentName = 0
    session = 0
    gradeList = []
    information = Instance.getInstance()
    collegeName = information.getCollegeName()
    collegeCode = information.getCollegeCode()
    semester = information.getSemester()
    subjectCodeList = information.getSubjects()

    listOfStrings = re.split('[ ]*[|][ ]*', line)
    listOfStrings = list(filter(None, listOfStrings))
    if line.find('SEMESTER EXAMINATION') != -1:
        pos = line.find('SEMESTER')
        semester = int(str(line)[pos-4])
        information.setSemester(semester)
    elif str(line).find('College') == 0:
        collegeCode = line[9:13]
        collegeName = line[15:len(line)]
        information.setCollegeCode(collegeCode)
        information.setCollegeName(collegeName)
    elif len(listOfStrings) > 4 and listOfStrings[0]!='100':
        if str(listOfStrings[0]) == 'ROLL NO':
            subjectCodeList.clear()
            if semester != 8:
                for eachSubjectCode in listOfStrings[4:len(listOfStrings)]:
                    try:
                        subjectCodeList.append(int(eachSubjectCode))
                    except ValueError:
                        print(eachSubjectCode)


            else:
                for eachSubjectCode in listOfStrings[4:9]:
                    subjectCodeList.append(int(eachSubjectCode))
        else:
            registrationNumber = listOfStrings[1]
            session = listOfStrings[2][0:4]
            studentName = listOfStrings[3]
            for eachGrade in listOfStrings[4:len(listOfStrings)]:
                try:
                    float(eachGrade)
                except ValueError:
                    gradeList.append(eachGrade)

            if(studentName == ''):
                studentName = "NO NAME"
            if semester == 8:
                subjectCodeList[-1] = 499
                if len(gradeList) == 4:
                    del subjectCodeList[3]
            information.setName(studentName)
            information.setRegistrationNumber(registrationNumber)
            information.setSession(session)
            information.setSemester(semester)
            information.setSubjects(subjectCodeList)
            information.setRrsults(gradeList)
            information.setCollegeCode(collegeCode)
            information.setCollegeName(collegeName)
            if(len(gradeList) == len(subjectCodeList)):
                databaseActivity(information)
            #print(information.getName())

