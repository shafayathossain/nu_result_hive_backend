from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter, SimpleRouter
from rest_framework_jwt.views import refresh_jwt_token, ObtainJSONWebToken

from .views.Default import Default
from .views.SessionsView import Sessions
from .views.UpdateForceView import Update_force
from .views.ImprovementView import Improvements
from .views.SocialLogin import Social_Login
from .views.PasswordResetView import PasswordResetView
from .views.PasswordResetDoneView import PasswordResetDoneView
from .views.ChangeProfileView import changeProfile
from .views.UpdateView import Update
from .views.VerifyEmailView import VerifyEmail
from .views.RegistrationFirstStepView import RegisterFirstStep
from .views.ResultsView import ResultsViewSet
from .views.SearchView import SearchViewSet
from .views.UserProfileView import UserProfileViewSet
from .views import views
from .views.RankView import RankViewSet, DevRankViewSet

router = SimpleRouter()
router.register('rank', RankViewSet, base_name='rank')
router.register('dev-rank', DevRankViewSet, base_name='rank')
router.register('results', ResultsViewSet, base_name='results')
router.register('search', SearchViewSet, base_name='search')


urlpatterns = [
    url(r'', include(router.urls)),
    url(r'^profile/$', UserProfileViewSet.as_view(), name='profile'),
    url(r'^profile/(?:id-(?P<id>\d+)/)?$', UserProfileViewSet.as_view(), name='profile'),
    url(r'^profile/(?P<id>[0-9]+)/$', UserProfileViewSet.as_view(), name='profile'),
    url(r'^login/$', ObtainJSONWebToken.as_view()),
    url(r'^api-token-refresh/$', refresh_jwt_token),
    url(r'^password-reset/', PasswordResetView.as_view(), name='password_reset'),
    url(r'password-reset-done/$', PasswordResetDoneView.as_view(), name='password_reset_confirm'),
    url(r'^first-step', RegisterFirstStep.as_view(), name='first-step'),
    url(r'^second-step', VerifyEmail.as_view(), name='second-step'),
    url(r'^update/(?:link-<string>/)?$', Update.as_view()),
    url(r'^change-profile', changeProfile.as_view()),
    url(r'^login-with/', Social_Login.as_view()),
    url(r'update-force/(?:link-<string>/)?$', Update_force.as_view()),
    url(r'^improvement/(?P<registration_number>.+)/$', Improvements.as_view()),
    url(r'^sessions/$', Sessions.as_view()),
    url(r'', Default.as_view())
]