from django.contrib.auth import authenticate
from rest_framework import serializers, status
from django.contrib.auth.hashers import make_password, check_password
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_409_CONFLICT, HTTP_500_INTERNAL_SERVER_ERROR
from rest_framework_jwt.serializers import JSONWebTokenSerializer, jwt_payload_handler, jwt_encode_handler
from django.core.mail import send_mail


from . import models

class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.student
        fields = ('id', 'name', 'registration_number', 'college', 'session')

class SemesterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.semester
        fields = ('id', 'semester', 'sgpa', 'grade', 'is_fail',)


class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.result
        fields = ('id', 'subject', 'result',)


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.subject
        fields = ('name', 'credit')


class SemesterInfoSerializer(serializers.ListSerializer):
    class Meta:
        model = models.semester
        fields = ('id', 'semester', 'sgpa', 'grade', 'is_fail','url',)


class RankSerializer(serializers.Serializer):
    name = serializers.CharField(read_only=True)
    college = serializers.CharField(read_only=True)
    registration_number = serializers.CharField(read_only=True)
    SGPA = serializers.CharField()

class DevRankSerializer(serializers.Serializer):
    name = serializers.CharField(read_only=True)
    college = serializers.CharField(read_only=True)
    registration_number = serializers.CharField(read_only=True)
    SGPA = serializers.FloatField(read_only=True)

class SearchSerializer(serializers.Serializer):
    name = serializers.CharField(read_only=True)
    registration_number = serializers.CharField(read_only=True)
    session = serializers.CharField(read_only=True)
    college = serializers.CharField(source='college.name', read_only=True)


class ResultsSerializer(serializers.Serializer):
    name = serializers.CharField(read_only=True)
    registration_number = serializers.CharField(read_only=True)
    session = serializers.CharField(read_only=True)
    college = serializers.CharField(read_only=True)
    college_code = serializers.IntegerField(read_only=True)
    semesters = serializers.ListField(read_only=True)
    semester = serializers.IntegerField(read_only=True)
    SGPA = serializers.FloatField(read_only=True)
    grade = serializers.CharField(read_only=True)
    subject_name = serializers.IntegerField(read_only=True)
    subject_code = serializers.IntegerField(read_only=True)



class RankListSerializer(serializers.Serializer):
    #rank = serializers.CharField()
    RankSerializer()

class RankSerializerClass(serializers.ModelSerializer):
    rank = RankListSerializer()


class UserProfileSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255)
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=255)
    reg_no = serializers.CharField(max_length=255, default=None)
    contact_no = serializers.CharField(max_length=255, default=None)

    def bind(self, field_name, parent):
        print(self.initial_data)

    def update(self, validate_data):
        models.UserProfile.objects.filter(id=validate_data['id']).update(name=validate_data['name'],
                                                                         password=validate_data['password'],
                                                                         reg_no=validate_data['reg_no'],
                                                                         contact_no=validate_data['contact_no'],
                                                                         is_active=True)

class FirstStepSerializer(serializers.Serializer):

    email = serializers.EmailField()
    name = serializers.CharField(max_length=255, default=None)
    token = serializers.CharField(max_length=255)
    is_active = serializers.BooleanField()

    def is_valid(self, raise_exception=False):
        is_active = False
        name = None
        print(models.UserProfile.objects.filter(email=self.initial_data['email']).count())

        email_obj = models.UserProfile.objects.filter(email=self.initial_data['email'])
        for obj in email_obj:
            is_active = obj.is_active

        if is_active:
            return False
        else:
            return True

    def create(self, validated_data):
        return models.UserProfile.objects.create(**validated_data)
    #
    # def update(self):
    #     return models.UserProfile.objects.update(is_verified = True)

