import random

from django.core.mail import EmailMessage
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from result_api import permission, serializer, models


class RegisterFirstStep(APIView):
    permission_classes = [AllowAny, permission.IsUpdateProfile, ]

    def post(self, request, **kwargs):
        if (len(request.data['email']) < 6) or (request.data['email'] == '') or ('@' not in request.data['email']) or (
                '.com' not in request.data['email']):
            return Response({"detail": "enter valid email address"},
                            status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)

        string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        token = ''
        data = {}
        ticket = ''
        for i in range(0, 6):
            ticket = ticket + random.choice(string)
        data['email'] = request.data.get('email')
        data['ticket'] = ticket
        serializerC = serializer.FirstStepSerializer(data=data)
        if serializerC.is_valid():
            email_obj = models.UserProfile.objects.filter(email=data['email'])
            if email_obj:
                for obj in email_obj:
                    ticket = obj.ticket
            else:
                serializerC.create(data)

            email = EmailMessage('Email Varification For ',
                                 'Thank you for using Result Hive. Please confirm email address by the ticket number provided following. Without varifying email address you cannot use the app. Provide the number on the ticket number field in the application.\n\n Ticket number: {}\n\nGreating from our team.'.format(
                                     ticket), to=[data['email']])
            email.send()
            return Response({"message": "check email for ticket number"})
        else:
            return Response({'detail': " email already exists with an account"}, status=status.HTTP_409_CONFLICT)
