from django.db import connection
from django.db.models import Avg, Sum, Count, F, Max, FloatField
from django.db.models.expressions import RawSQL
from rest_framework import generics, viewsets
from rest_framework.permissions import AllowAny

from result_api import serializer
from result_api import models


class RankViewSet(generics.mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    permission_classes = [AllowAny]

    def get_queryset(self):
        session = self.request.query_params.get('session')[0:4]
        semester = self.request.query_params.get('semester')
        print(session)
        print(semester)
        with connection.cursor() as cursor:
            query = ""
            if semester == 'overall':
                query = "SELECT student.name, student.registration_number, college.name as college, SUM(sgpa)/COUNT(semester.semester) as cgpa" \
                        " FROM student, college, semester WHERE student.id=semester.student_id AND " \
                        "student.college_id=college.id AND student.session=%s GROUP BY student.id ORDER BY `cgpa`  " \
                        "DESC" % (session)
            else:
                query = "SELECT student.name, student.registration_number, college.name as college,  semester.sgpa " \
                        "FROM student, semester, college WHERE student.id=semester.student_id AND student.session='%s' " \
                        "AND semester.semester=%d AND college.id=student.college_id GROUP BY student.id ORDER BY sgpa " \
                        "DESC" % (session, int(semester))
            cursor.execute(query)
            row = cursor.fetchall()
        print(row)
        queryList = []
        dict_of_keys = []
        dict_of_keys = ['name', 'registration_number', 'college', 'SGPA']
        for tuple in row:
            queryList.append({dict_of_keys[i]: tuple[i] for i, _ in enumerate(tuple)})
        return queryList

    serializer_class = serializer.RankSerializer


class DevRankViewSet(generics.mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    permission_classes = [AllowAny]
    pagination_class = None

    def get_queryset(self):
        session = self.request.query_params.get('session')[0:4]
        semester = self.request.query_params.get('semester')
        if semester == 'overall':
            queryset =  models.semester.objects.select_related('student__college')\
                .values('student__id', 'student', 'student__college')\
                .annotate(SGPA=Avg('sgpa')) \
                .annotate(ID=F('student__id')) \
                .annotate(name=F('student__name')) \
                .annotate(college=F('student__college__name')) \
                .annotate(registration_number=F('student__registration_number')) \
                .filter(student__session=session)\
                .order_by('-SGPA')
            # for i in queryset:
            #     print(i)
            return queryset
        else:
            return models.semester.objects.select_related('student__college')\
                .filter(student__session=session, semester=semester) \
                .annotate(SGPA=F('sgpa')) \
                .annotate(ID=F('student__id')) \
                .annotate(name=F('student__name')) \
                .annotate(college=F('student__college__name')) \
                .annotate(registration_number=F('student__registration_number')) \
                .order_by('-SGPA')



    serializer_class = serializer.DevRankSerializer
